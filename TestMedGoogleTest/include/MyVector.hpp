#ifndef __MY_VECTOR_HPP__
#define __MY_VECTOR_HPP__

#include <stdlib.h>
#include <iterator>
#include <iostream>
#include <algorithm>

template< typename T >
class MyVectorIterator : public std::iterator<std::input_iterator_tag, T >
{
	public:
	MyVectorIterator( T* x )
	{
		p = x;
	}

	MyVectorIterator( const MyVectorIterator& mit )
	{
		p = mit.p;
	}

	MyVectorIterator& operator++()
	{
		p++;
		return *this;
	}

	MyVectorIterator operator++( int )
	{
		MyVectorIterator tmp( *this );
		operator++();
		return tmp;
	}


	bool operator==( const MyVectorIterator& rhs ) const
	{
		return p == rhs.p;
	}

	bool operator!=( const MyVectorIterator& rhs ) const
	{
		return p != rhs.p;
	}

	T& operator*()
	{
		return *p;
	}


	private:
	T* p;
};





template< typename T >
class MyVector
{
public:
    explicit MyVector( int capacity = 10 )
    {
        data_ = new T[capacity];
        vectorSize = capacity;
        for (size_t i = 0; i < vectorSize; i++)
        {
            data_[ i ] = capacity;
        }
        
    }

    MyVector( const MyVector& other )
    {
        // Allocate as much space as the "other" one.
        vectorSize = other.vectorSize;
        data_ = new T[ vectorSize ];

        // Create temporary object to copy into.
        MyVector< T > temp( vectorSize );

        // Create vector iterators.
        MyVectorIterator< T > begin( other.data_ );
        MyVectorIterator< T > end( &(other.data_[other.vectorSize]) );
        MyVectorIterator< T > to( temp.data_ );

        // Copied into the temp object.
        std::copy( begin, end, to );

        // Now swaps "this" and the temp.
        temp.swap( *this );

        // The temp pointer will be deleted upon exiting this scope..

    }


    void swap( MyVector& other )
    {
        T* temp = data_;

        data_ = other.begin();
        other.data_ = temp;
    }

    void print( void )
    {
        for (size_t i = 0; i < vectorSize; i++)
        {
            std::cout << data_[ i ] << "\n";
        }
        
    }

    T& whatVal( size_t slot ) const
    {
        return data_[ slot ];
    }

    MyVector& operator=( const MyVector& other )
    {
        // Create temp object.
        MyVector< T > temp( other.MyVector< T >::vectorSize() );

        // Copy to temp object.
        // Create vector iterators.
        MyVectorIterator< T > begin( other.MyVector< T >::begin() );
        MyVectorIterator< T > end( other.MyVector< T >::end() );
        MyVectorIterator< T > to( temp.MyVector< T >::begin() );

        // Swap this and temp object.
        temp.swap( *this );
        vectorSize = other.vectorSize();

        // Destruction of previous data happens with the destruction of temp.
        return *this;
    }

    ~MyVector()
    {
        delete data_;
        vectorSize = 0;
    }

    int size( void ) const
    {
        return vectorSize;
    }

    T& back( void );

    void push_back( const T& t ) // Kopierer da det er const. Ellers havde den brugt MOVE
    {
        // Create temp vector object
        MyVector< T > temp( vectorSize + 1 );

        // Add an object to temp vector.
        // Create vector iterators.
        MyVectorIterator< T > begin( MyVector< T >::begin() );
        MyVectorIterator< T > end( MyVector< T >::end() );
        MyVectorIterator< T > to( temp.MyVector< T >::begin() );

        // Copy content of this into temp.
        std::copy( begin, end, to );

        // Then add an object to temp.
        // Remember that this is a copy, not the actual obj.
        temp.begin()[ vectorSize ] = t;

        // Now swap the containers, so that temp can be deleted.
        temp.swap( *this );
        vectorSize++;
    }

    void pop_back( void )
    {
        // Create temp object
        MyVector< T > temp( vectorSize - 1 );

        // Create vector iterators.
        MyVectorIterator< T > begin( MyVector< T >::begin() );
        MyVectorIterator< T > end( MyVector< T >::end() - 1 );
        MyVectorIterator< T > to( temp.MyVector< T >::begin() );

        // Copes content
        std::copy( begin, end, to );

        // Now just swap the data pointers.
        temp.swap( *this );

        // Decrement arraySize by 1
        vectorSize--;
    }

    void insert( const T& t, size_t n )
    {
        // Create temp object
        MyVector< T > temp( vectorSize );

        // Create vector iterators.
        MyVectorIterator< T > begin( MyVector< T >::begin() );
        MyVectorIterator< T > end( MyVector< T >::end() );
        MyVectorIterator< T > to( temp.MyVector< T >::begin() );

        // Create an iterator to the point in the data_.
        MyVectorIterator< T > exceptMe( MyVector< T >::begin() + n );

        // Copy content to temp, unless it's on the n'th index.
        std::copy( begin, end, to );

        temp[ n ] = t;

         // Now swap
        temp.swap( *this );
    }

    T* begin( void )
    {
        return data_;
    }

    T* end( void )
    {
        return &data_[ vectorSize ];
    }

    T& operator[]( size_t i )
    {
        return data_[ i ];
    }

    int getDataSpot( void )
    {
        return dataSpot;
    }

private:
    T* data_;
    size_t vectorSize;
    int dataSpot = 0;
};



template< typename T >
bool operator==( const MyVector< T >& first, const MyVector< T >& second )
{
    for( size_t i = 0; i < first.size(); i++ )
    {
        if ( first.whatVal( i ) != second.whatVal( i ) )
        {
            return false;
        }
    }
    if( first.size() != second.size() )
    {
        return false;
    }
    return true;
}




#endif //__MY_VECTOR_HPP__