#include "../include/MockTurtle.hpp"
#include "../googletest/googletest/src/gtest-all.cc"

using ::testing::AtLeast;

TEST( PainterTest, CanDrawSomething )
{
    MockTurtle turtle;
    EXPECT_CALL( turtle, PenDown() ).Times( 2 );

    Painter painter( &turtle );

    EXPECT_TRUE( painter.DrawCircle( 0, 0, 100 ) );
    EXPECT_TRUE( painter.DrawCircle( 0, 0, 100 ) );
    //EXPECT_TRUE( painter.DrawCircle( 0, 0, 100 ) );
}


int main( int argc, char** args )
{
    testing::InitGoogleMock( &argc, args );
    return RUN_ALL_TESTS();
}